# Define functions (not for running and calling)

# def make_dough_option(arg1, arg2):
#     if 'flour' in arg1 and 'water' in arg2:
#         return 'dough'
#     else:
#         return 'not dough'

# User story 2 function
def make_pao(arg1, arg2):
    if 'dough' in arg1 and 'oven' in arg2:
        return 'Pao'
    else:
        return 'not Pao'