# Pao factory TDD project

This project is to explore the coding of a bread factory(pao).

We will cover topics including:

- git,bitbucket
- Markdown
- Agile / scrum principles
- TDD (Test Driven Development)
- Unit test
- Functional programming


Other principles:

- Separation of concerns
- DRY code

### TDD & Unit tests

Unit test, is a single test that tests a part of a function.
Several tests together will help ensure the functionality of the program, reduce technical debt and keep maintainable.

Generally code lives, entropy adds complexity and short cuts lead to technical debt that can kill a project.

Technical debt is the concept of taking shortcuts - like skipping documentation or not making unit tests- leading to unmanageable code.
Other factors such as time, people leaving, updates also add to technical debt.

**Test driven development** Is a way of developing code that is very 


### User Stories and tests

Good User stories can be used to make your unit test. These are usually done by the `Three Amigos` `Devs`+ `testers`+ `business analyst`.

In our case it will just be us.

**User Story1** As a bread maker, I want to provide `flour` and `water` to make my `make_dough_option` and get out `dough`, else i wnt `not dough`. So that i can then bake the bread

**User Story2** As a bread maker, I want to be able to take `dough` and user the `bake_option` to get `Pao`. Else i should get `not Pao`. So that i can make bread.

**User Story 3** As a bread maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `Pao`, else give me `not Pao`. So i can make bread with one simple action