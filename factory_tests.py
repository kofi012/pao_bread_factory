# # Import functions and run tests
from factory_functions import *

# # What is a test? It's an assertion, with a known outcome of a function that comes back either True/False. that comes back with True or False

# # **User Story1** As a bread maker, I want to provide `flour` and `water` to make my `make_dough_option` and get out `dough`, else i wnt `not dough`. So that i can then bake the bread.
# print("test number 1 - make_dough_option with 'flour' and 'water should equal 'dough'")
# print(make_dough_option('(flour','water') == 'dough')
# print("test number 2 - make_dough_option with 'cement' and 'water' should equal 'not dough'")

# print(make_dough_option('cement', 'water')== 'not dough')

# User story 2 + make tests + functions
# **User Story2** As a bread maker, I want to be able to take `dough` and use the `bake_option` to get `Pao`. Else i should get `not Pao`. So that i can make bread.

print("test number 1 - make_dough_option with 'dough' and 'oven' should equal 'Pao'")
print(make_pao('(dough','oven') == 'Pao')
print("test number 2 - make_dough_option with 'cement' and 'dough' should equal 'not Pao'")

print(make_pao('cement', 'dough') == 'not Pao')

# User story 3 + make tests + functions
# **User Story 3** As a bread maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `Pao`, else give me `not Pao`. So i can make bread with one simple action